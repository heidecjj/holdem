# Holdem [![pipeline status](https://gitlab.com/heidecjj/holdem/badges/master/pipeline.svg)](https://gitlab.com/heidecjj/holdem/commits/master)
Know when to hold 'em.

A Texas Hold 'Em Hand Predictor. Developed for Rose-Hulman's ECE497-01 CUDA Programming on GPU. For an in-depth discussion, see my [final paper](./CUDA-Accelerated-Texas-Holdem-Hand-Predictor.pdf).

Holdem takes the state of a Texas Hold 'Em game and gives the user the probability they will win along with their probability of getting each of the 10 standard poker hands (high card through royal flush).
Holdem has two different strategies for simulating Texas Hold 'Em games, a single-threaded CPU strategy and a much faster CUDA strategy.

Branches 7-optimize-atomic-operations and 8-optimize-deck-storage contain attempted optimizations to the CUDA strategy that ended up slowing down execution times.

## Getting Started
### Prerequisites
You need to make sure to have the following installed before building Holdem:
- make
- gcc
- CUDA Toolkit
- Boost Program Options

### Building
Building is simple:
```bash
cd build
make
```
This will create 2 executible files, holdem and test, along with some object files in the build directory.

### Running Holdem
From the build directory, run:
```bash
./holdem --help
```
This will list the options for running holdem.

### Running the tests
From the build directory, run:
```bash
./test
```