#include "gtest/gtest.h"
#include "../src/cards.h"

namespace cards {
  TEST(SuitString, Valid) {
    EXPECT_EQ(cards::Card(2, cards::kDiamond).suitString(), "D");
    EXPECT_EQ(cards::Card(3, cards::kHeart).suitString(), "H");
    EXPECT_EQ(cards::Card(3, cards::kClub).suitString(), "C");
    EXPECT_EQ(cards::Card(3, cards::kSpade).suitString(), "S");
  }

  TEST(Constructor, InvalidSuit) {
    for (int i = 0; i < 256; i++) {
      if (cards::kDiamond != i
          && cards::kHeart != i
          && cards::kClub != i
          && cards::kSpade != i) {
        ASSERT_THROW(cards::Card(2, i), std::invalid_argument);
      }
    }
  }


  TEST(NumberString, Valid) {
    EXPECT_EQ(cards::Card(cards::kAce, cards::kSpade).numberString(), "A");
    EXPECT_EQ(cards::Card(cards::kKing, cards::kSpade).numberString(), "K");
    EXPECT_EQ(cards::Card(cards::kQueen, cards::kSpade).numberString(), "Q");
    EXPECT_EQ(cards::Card(cards::kJack, cards::kSpade).numberString(), "J");
  }

  TEST(Constructor, InvalidNumber) {
    for (int i = 1; i < 256; i++) {
      if (i < 2 || i > cards::kAce) {
        ASSERT_THROW(cards::Card(i, cards::kSpade), std::invalid_argument);
      }
    }
  }
}