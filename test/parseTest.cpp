#include <vector>
#include "gtest/gtest.h"
#include "../src/cards.h"
#include "../src/parse.h"

namespace cards {
    TEST(CardFromString, Valid) {
        ASSERT_EQ(parse::cardFromString("4-h"), Card(4, kHeart));
        ASSERT_EQ(parse::cardFromString("4-H"), Card(4, kHeart));
        ASSERT_EQ(parse::cardFromString("A-h"), Card(kAce, kHeart));
        ASSERT_EQ(parse::cardFromString("a-H"), Card(kAce, kHeart));
        ASSERT_EQ(parse::cardFromString("k-h"), Card(kKing, kHeart));
        ASSERT_EQ(parse::cardFromString("10-H"), Card(10, kHeart));

        ASSERT_EQ(parse::cardFromString("a-s"), Card(kAce, kSpade));
        ASSERT_EQ(parse::cardFromString("k-D"), Card(kKing, kDiamond));

        ASSERT_EQ(parse::cardFromString("a-c"), Card(kAce, kClub));
        ASSERT_EQ(parse::cardFromString("k-C"), Card(kKing, kClub));
    }

    TEST(CardFromString, Invalid) {
        EXPECT_ANY_THROW(parse::cardFromString("a-cc"));
        EXPECT_ANY_THROW(parse::cardFromString("0-c"));
        EXPECT_ANY_THROW(parse::cardFromString("04-c"));
        EXPECT_ANY_THROW(parse::cardFromString("11-c"));
        EXPECT_ANY_THROW(parse::cardFromString("15-c"));
        EXPECT_ANY_THROW(parse::cardFromString("josh-c"));
        EXPECT_ANY_THROW(parse::cardFromString("kj-c"));
        EXPECT_ANY_THROW(parse::cardFromString("4--c"));
        EXPECT_ANY_THROW(parse::cardFromString("4-?c"));
        EXPECT_ANY_THROW(parse::cardFromString("4?c"));
        EXPECT_ANY_THROW(parse::cardFromString("4-c 8-s"));
        EXPECT_ANY_THROW(parse::cardFromString("4-c8-s"));
    }

    TEST(CardFromString, Vector) {
        std::vector<std::string> cardStrings {"4-c", "A-S"};
        std::vector<Card> expectedCards {Card(4, kClub), Card(kAce, kSpade)};
        EXPECT_EQ(expectedCards, parse::cardFromString(cardStrings));
        
        EXPECT_TRUE(parse::cardFromString(std::vector<std::string>()).empty());
    }

    TEST(CheckDuplicates, Unique) {
        EXPECT_FALSE(parse::checkDuplicates({Card(4, kClub), Card(kAce, kSpade)}, {}));
        EXPECT_FALSE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(5, kSpade)}));
        EXPECT_FALSE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(5, kSpade), Card(5, kHeart)}));
        EXPECT_FALSE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(5, kSpade), Card(5, kHeart), Card(5, kDiamond)}));
    }

    TEST(CheckDuplicates, Duplicate) {
        EXPECT_TRUE(parse::checkDuplicates({Card(4, kClub), Card(4, kClub)}, {}));
        EXPECT_TRUE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(4, kSpade)}));
        EXPECT_TRUE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(4, kDiamond)}));
        EXPECT_TRUE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(4, kClub), Card(5, kHeart)}));
        EXPECT_TRUE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kDiamond), Card(6, kClub), Card(4, kHeart)}));
        EXPECT_TRUE(parse::checkDuplicates({Card(4, kClub), Card(4, kSpade)}, {Card(4, kHeart), Card(4, kHeart), Card(4, kHeart), Card(4, kHeart), Card(4, kHeart)}));
    }
}