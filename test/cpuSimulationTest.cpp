#include "gtest/gtest.h"
#include "../src/cpuSimulation.h"
#include "../src/simulation.h"
#include "../src/cards.h"
#include "../src/texas.h"

TEST(RunCpuSimulation, SinglePlayer) {
    simulation::CpuSimulation cpuSim(1);
    texas::Hole myHole{cards::Card(cards::kAce, cards::kDiamond), cards::Card(cards::kAce, cards::kHeart)};
    
    ASSERT_EQ(1, cpuSim.winningChance(myHole, {}));
}