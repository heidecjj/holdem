#include <vector>
#include "gtest/gtest.h"
#include "../src/cards.h"
#include "../src/texas.h"

TEST(FindStraight, Exists) {
  cards::Number high;
  std::vector<cards::Number> numbers = {2, 3, 4, 5, 6, 8, 9};

  ASSERT_TRUE(texas::findStraight(&numbers[0], 7, &high));
  ASSERT_EQ(high, 6);

  numbers = {2, 3, 4, 5, 10, 10, cards::kAce};
  ASSERT_TRUE(texas::findStraight(&numbers[0], 7, &high));
  ASSERT_EQ(high, 5);

  numbers = {2, 2, 3, 4, 5, 10, cards::kAce};
  ASSERT_TRUE(texas::findStraight(&numbers[0], 7, &high));
  ASSERT_EQ(high, 5);

  numbers = {6, 7, 8, 8, 8, 9, 10};
  ASSERT_TRUE(texas::findStraight(&numbers[0], 7, &high));
  ASSERT_EQ(high, 10);

  numbers = {10, cards::kJack, cards::kQueen, cards::kKing, cards::kAce};
  ASSERT_TRUE(texas::findStraight(&numbers[0], 5, &high));
  ASSERT_EQ(high, cards::kAce);

  numbers = {10, cards::kJack, cards::kQueen, cards::kKing, cards::kAce, cards::kAce};
  ASSERT_TRUE(texas::findStraight(&numbers[0], 6, &high));
  ASSERT_EQ(high, cards::kAce);

  numbers = {2, 3, 4, 5, cards::kJack, cards::kQueen, cards::kKing, cards::kAce};
  ASSERT_TRUE(texas::findStraight(&numbers[0], 8, &high));
  ASSERT_EQ(high, 5);
}

TEST(findStraight, DoesNotExist) {
  cards::Number high;
  std::vector<cards::Number> numbers = {2, 3, 5, 6, 8, 9, cards::kAce};

  ASSERT_FALSE(texas::findStraight(&numbers[0], 7, &high));

  numbers = {2, 4, 6, 8, 10};
  ASSERT_FALSE(texas::findStraight(&numbers[0], 5, &high));

  numbers = {2, 3, 4, 5};
  ASSERT_FALSE(texas::findStraight(&numbers[0], 4, &high));

  numbers = {2, 4, 5, 6, 7, 9, 10};
  ASSERT_FALSE(texas::findStraight(&numbers[0], 7, &high));
}


TEST(FindFlushSuit, Exists) {
  cards::Suit suit;
  int count;
  std::vector<cards::Card> cards = 
    {cards::Card(cards::kAce, cards::kSpade),
	 cards::Card(cards::kAce, cards::kSpade),
	 cards::Card(cards::kAce, cards::kSpade),
	 cards::Card(cards::kAce, cards::kSpade),
	 cards::Card(cards::kAce, cards::kSpade),
	 cards::Card(cards::kAce, cards::kSpade),
	 cards::Card(cards::kAce, cards::kSpade)};
  ASSERT_TRUE(texas::findFlushSuit(&cards[0], &suit, &count));
  ASSERT_EQ(suit, cards::kSpade);
  ASSERT_EQ(count, 7);

  cards = 
    {cards::Card(9, cards::kSpade),
     cards::Card(9, cards::kHeart),
     cards::Card(cards::kKing, cards::kHeart),
	 cards::Card(2, cards::kSpade),
	 cards::Card(5, cards::kSpade),
	 cards::Card(7, cards::kSpade),
	 cards::Card(6, cards::kSpade)};
  ASSERT_TRUE(texas::findFlushSuit(&cards[0], &suit, &count));
  ASSERT_EQ(suit, cards::kSpade);
  ASSERT_EQ(count, 5);

  cards = 
    {cards::Card(9, cards::kDiamond),
     cards::Card(9, cards::kDiamond),
     cards::Card(cards::kKing, cards::kHeart),
	 cards::Card(2, cards::kSpade),
	 cards::Card(5, cards::kDiamond),
	 cards::Card(7, cards::kDiamond),
	 cards::Card(6, cards::kDiamond)};
  ASSERT_TRUE(texas::findFlushSuit(&cards[0], &suit, &count));
  ASSERT_EQ(suit, cards::kDiamond);
  ASSERT_EQ(count, 5);

  cards = 
    {cards::Card(9, cards::kDiamond),
     cards::Card(9, cards::kDiamond),
     cards::Card(cards::kKing, cards::kHeart),
	 cards::Card(2, cards::kDiamond),
	 cards::Card(5, cards::kDiamond),
	 cards::Card(7, cards::kDiamond),
	 cards::Card(6, cards::kDiamond)};
  ASSERT_TRUE(texas::findFlushSuit(&cards[0], &suit, &count));
  ASSERT_EQ(suit, cards::kDiamond);
  ASSERT_EQ(count, 6);
}

TEST(FindFlushSuit, DoesNotExist) {
  cards::Suit suit;
  int count;
  std::vector<cards::Card> cards = 
    {cards::Card(9, cards::kDiamond),
     cards::Card(9, cards::kHeart),
     cards::Card(cards::kKing, cards::kHeart),
	 cards::Card(2, cards::kSpade),
	 cards::Card(5, cards::kSpade),
	 cards::Card(7, cards::kSpade),
	 cards::Card(6, cards::kSpade)};
  ASSERT_FALSE(texas::findFlushSuit(&cards[0], &suit, &count));

  cards = 
    {cards::Card(9, cards::kDiamond),
     cards::Card(9, cards::kHeart),
     cards::Card(cards::kKing, cards::kHeart),
	 cards::Card(2, cards::kDiamond),
	 cards::Card(5, cards::kSpade),
	 cards::Card(7, cards::kClub),
	 cards::Card(6, cards::kSpade)};
  ASSERT_FALSE(texas::findFlushSuit(&cards[0], &suit, &count));

  cards = 
    {cards::Card(9, cards::kDiamond),
     cards::Card(9, cards::kHeart),
     cards::Card(cards::kKing, cards::kHeart),
	 cards::Card(2, cards::kClub),
	 cards::Card(5, cards::kSpade),
	 cards::Card(7, cards::kHeart),
	 cards::Card(6, cards::kHeart)};
  ASSERT_FALSE(texas::findFlushSuit(&cards[0], &suit, &count));
}

TEST(FindBestHand, HighCard) {
  //King high in hole
  texas::Hole hole = {
      cards::Card(cards::kKing, cards::kSpade),
      cards::Card(9, cards::kDiamond)
  };
  texas::Community community = {
      cards::Card(10, cards::kDiamond),
      cards::Card(2, cards::kSpade),
      cards::Card(4, cards::kHeart),
      cards::Card(6, cards::kClub),
      cards::Card(8, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kHigh, cards::kKing, 0, cards::kKing, 9);
  ASSERT_EQ(hand, expected);

  // 10 high in community
  hole[0] = cards::Card(7, cards::kSpade);
  hole[1] = cards::Card(3, cards::kDiamond);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kHigh, 10, 0, 7, 3);
  ASSERT_EQ(hand, expected);
}

TEST(FindBestHand, Pair) {
  // Pair of kings in hole
  texas::Hole hole = {
      cards::Card(cards::kKing, cards::kSpade),
      cards::Card(cards::kKing, cards::kDiamond)
  };
  texas::Community community = {
      cards::Card(10, cards::kDiamond),
      cards::Card(2, cards::kSpade),
      cards::Card(4, cards::kHeart),
      cards::Card(6, cards::kClub),
      cards::Card(8, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kPair, cards::kKing, 0, cards::kKing, cards::kKing);
  ASSERT_EQ(hand, expected);

  // Pair of 10s half hole
  hole[0] = cards::Card(10, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kPair, 10, 0, cards::kKing, 10);
  ASSERT_EQ(hand, expected);

  // Pair of 4s in community
  hole[0] = cards::Card(7, cards::kSpade);
  hole[1] = cards::Card(3, cards::kDiamond);
  community[1] = cards::Card(4, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kPair, 4, 0, 7, 3);
  ASSERT_EQ(hand, expected);
}

TEST(findBestHand, TwoPair) {
  // Pair of kings in hole and 4s in community
  texas::Hole hole = {
      cards::Card(cards::kKing, cards::kSpade),
      cards::Card(cards::kKing, cards::kDiamond)
  };
  texas::Community community = {
      cards::Card(10, cards::kDiamond),
      cards::Card(4, cards::kSpade),
      cards::Card(4, cards::kHeart),
      cards::Card(6, cards::kClub),
      cards::Card(8, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kTwoPair, cards::kKing, 4, cards::kKing, cards::kKing);
  ASSERT_EQ(hand, expected);

  // Pair of 10s half hole and 4s in community
  hole[0] = cards::Card(10, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kTwoPair, 10, 4, cards::kKing, 10);
  ASSERT_EQ(hand, expected);

  // Pair of 6s and 4s in community
  hole[0] = cards::Card(7, cards::kSpade);
  hole[1] = cards::Card(3, cards::kDiamond);
  community[4] = cards::Card(6, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kTwoPair, 6, 4, 7, 3);
  ASSERT_EQ(hand, expected);
}

TEST(findBestHand, ThreeKind) {
  // Three 7's 2 in hole
  texas::Hole hole = {
      cards::Card(7, cards::kSpade),
      cards::Card(7, cards::kDiamond)
  };
  texas::Community community = {
      cards::Card(10, cards::kDiamond),
      cards::Card(4, cards::kSpade),
      cards::Card(7, cards::kHeart),
      cards::Card(6, cards::kClub),
      cards::Card(8, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kThreeKind, 7, 0, 7, 7);
  ASSERT_EQ(hand, expected);

  // Three 7's 1 in hole
  hole[0] = cards::Card(cards::kKing, cards::kSpade);
  community[1] = cards::Card(7, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kThreeKind, 7, 0, cards::kKing, 7);
  ASSERT_EQ(hand, expected);

  // Three 7's 0 in hole
  hole[1] = cards::Card(3, cards::kDiamond);
  community[4] = cards::Card(7, cards::kDiamond);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kThreeKind, 7, 0, cards::kKing, 3);
  ASSERT_EQ(hand, expected);
}

TEST(FindBestHand, Straight) {
  // 2-6
  texas::Hole hole = {
      cards::Card(10, cards::kSpade),
      cards::Card(6, cards::kDiamond)
  };
  texas::Community community = {
      cards::Card(3, cards::kDiamond),
      cards::Card(4, cards::kSpade),
      cards::Card(cards::kJack, cards::kHeart),
      cards::Card(2, cards::kClub),
      cards::Card(5, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kStraight, 6, 0, 10, 6);
  ASSERT_EQ(hand, expected);

  // A-5
  hole[1] = cards::Card(cards::kAce, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kStraight, 5, 0, cards::kAce, 10);
  ASSERT_EQ(hand, expected);

  // A-5 with a pair
  community[2] = cards::Card(3, cards::kHeart);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kStraight, 5, 0, cards::kAce, 10);
  ASSERT_EQ(hand, expected);

  // A-5 with a triple
  hole[0] = cards::Card(3, cards::kSpade);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kStraight, 5, 0, cards::kAce, 3);
  ASSERT_EQ(hand, expected);

  // A-5 with a two pair
  hole[0] = cards::Card(4, cards::kHeart);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kStraight, 5, 0, cards::kAce, 4);
  ASSERT_EQ(hand, expected);
}

TEST(FindBestHand, Flush) {
  //King high with club flush
  texas::Hole hole = {
      cards::Card(cards::kKing, cards::kClub),
      cards::Card(9, cards::kClub)
  };
  texas::Community community = {
      cards::Card(10, cards::kClub),
      cards::Card(2, cards::kSpade),
      cards::Card(4, cards::kHeart),
      cards::Card(6, cards::kClub),
      cards::Card(8, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kFlush, cards::kKing, 0, cards::kKing, 9);
  ASSERT_EQ(hand, expected);

  // ace high with club flush and 6 same suit cards
  community[1] = cards::Card(cards::kAce, cards::kClub);
  hand = texas::findBestHand(community, hole);
  expected = texas::Hand(texas::kFlush, cards::kAce, 0, cards::kKing, 9);
  ASSERT_EQ(hand, expected);
}

//also check high and pair
TEST(FindBestHand, FourKindFlush) {
  //King high with club flush
  texas::Hole hole = {
      cards::Card(cards::kAce, cards::kClub),
      cards::Card(cards::kAce, cards::kDiamond)
  };
  texas::Community community = {
      cards::Card(cards::kAce, cards::kHeart),
      cards::Card(cards::kAce, cards::kSpade),
      cards::Card(4, cards::kClub),
      cards::Card(6, cards::kClub),
      cards::Card(8, cards::kClub)
  };
  texas::Hand hand = texas::findBestHand(community, hole);
  texas::Hand expected = texas::Hand(texas::kFourKind, cards::kAce, 0, cards::kAce, cards::kAce);
  ASSERT_EQ(hand, expected);
}