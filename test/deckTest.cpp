#include <vector>
#include <algorithm>
#include "gtest/gtest.h"
#include "../src/cards.h"
#include "../src/deck.h"
#include "../src/random.h"

bool fiftyTwoUniqueCards(std::vector<cards::Card> cardVect) {
    std::vector<cards::Suit> suits = {cards::kClub, cards::kDiamond, cards::kHeart, cards::kSpade};
    std::vector<cards::Card> stdCards;
    for (auto suit: suits) {
        for (cards::Number i = 2; i <= cards::kAce; i++) {
            stdCards.push_back(cards::Card(i, suit));
        }
    }

    for (auto c: cardVect) {
        int index = -1;
        for (uint i = 0; i < stdCards.size(); i++) {
            if (c == stdCards[i]) {
                index = i;
            }
        }
        if (index == -1) {
            return false;
        }
        stdCards.erase(stdCards.begin() + index);
    }
    return stdCards.empty();
}

bool vectorNotIn(std::vector<cards::Card> t_vect1, std::vector<cards::Card> t_vect2) {
    bool notIn = true;
    for (auto c: t_vect1) {
        notIn &= (std::find(t_vect2.begin(), t_vect2.end(), c) == t_vect2.end());
    }
    return notIn;
}

TEST(NextCard, ProperlyShuffled) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);
    std::vector<cards::Card> shuffled;
    while(dk.hasNext()) {
        shuffled.push_back(dk.nextCard());
    }
    ASSERT_TRUE(fiftyTwoUniqueCards(shuffled));
}

TEST(RemoveCard, CardExists) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);
    cards::Card c1(2, cards::kClub);
    dk.removeCard(c1);
    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(51, leftover.size());
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), c1));
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), cards::Card(0, 0)));


    dk = deck::Deck(&cpuRand);
    cards::Card c2(cards::kAce, cards::kSpade);
    cards::Card c3(9, cards::kHeart);
    dk.removeCard(c1);
    dk.removeCard(c2);
    dk.removeCard(c3);

    leftover = std::vector<cards::Card>();
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }
    ASSERT_EQ(49, leftover.size());
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), c1));
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), c2));
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), c3));
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), cards::Card(0, 0)));
}

TEST(RemoveCard, CardDoesNotExist) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);
    cards::Card c1(0, cards::kClub);
    dk.removeCard(c1);
    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(52, leftover.size());
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), c1));
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), cards::Card(0, 0)));
}

TEST(RemoveCard, NullCard) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);
    dk.removeCard(cards::Card(0, 0));
    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(52, leftover.size());
    ASSERT_EQ(leftover.end(), std::find(leftover.begin(), leftover.end(), cards::Card(0, 0)));
}

TEST(RemoveCard, AllCards) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);
    
    for (cards::Suit s = cards::kDiamond; s <= cards::kSpade; s++) {
        for (cards::Number i = 2; i <= cards::kAce; i++) {
            dk.removeCard(cards::Card(i, s));
        }
    }

    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(0, leftover.size());
}

TEST(RemoveCard, AllCardsButOne) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);
    
    for (cards::Suit s = cards::kDiamond; s <= cards::kSpade; s++) {
        for (cards::Number i = 2; i <= cards::kAce; i++) {
            if (!(s == cards::kSpade && i == cards::kAce))
            dk.removeCard(cards::Card(i, s));
        }
    }

    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(1, leftover.size());
    ASSERT_EQ(cards::Card(cards::kAce, cards::kSpade), leftover[0]);
}

TEST(RemoveCards, ArraySize0) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);

    std::vector<cards::Card> toRemove;
    
    dk.removeCards(&toRemove[0], 0);

    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(52, leftover.size());
    ASSERT_TRUE(vectorNotIn(toRemove, leftover));

}

TEST(RemoveCards, ArraySize4) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);

    std::vector<cards::Card> toRemove 
        {cards::Card(2, cards::kDiamond), cards::Card(10, cards::kClub),
        cards::Card(4, cards::kHeart), cards::Card(cards::kKing, cards::kSpade)};

    dk.removeCards(&toRemove[0], 4);

    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(48, leftover.size());
    ASSERT_TRUE(vectorNotIn(toRemove, leftover));
}

TEST(RemoveCards, ArrayAllSpades) {
    randomGen::Random cpuRand = randomGen::Random();
    deck::Deck dk(&cpuRand);

    std::vector<cards::Card> toRemove;
    for (cards::Number i = 2; i <= cards::kAce; i++) {
        toRemove.push_back(cards::Card(i, cards::kSpade));
    }
    dk.removeCards(&toRemove[0], 13);

    std::vector<cards::Card> leftover;
    while(dk.hasNext()) {
        leftover.push_back(dk.nextCard());
    }

    ASSERT_EQ(39, leftover.size());
    ASSERT_TRUE(vectorNotIn(toRemove, leftover));
}

