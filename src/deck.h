// deck.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Abstracts a standard deck of 52 cards. Cards are dealt
// in a random order and are lazily shuffled. Specific cards
// can also be removed from the deck.

#ifndef DECK_H
#define DECK_H

#include <cuda_runtime.h>

#include "random.h"
#include "cards.h"

#define DECK_SIZE 52

namespace deck {

    // Abstracts a standard deck of 52 cards. Cards are dealt
    // in a random order. Specific cards can also be removed
    // from the deck.
    class Deck {
        private:
            // Stores the cards that this deck may deal.
            // Also contains cards that have already been
            // dealt.
            cards::Card available[DECK_SIZE];

            // Keeps track of the next card in available[]
            // to be dealt
            uint8_t currentCard = 0;

            // For generation of random integers.
            randomGen::Random *rand;

            // Advances currentCard past any "NULL" cards in
            // available[]
            __host__ __device__
            void skipNullCards();

        public:
            // Constructs a deck of 52 cards given a source
            // of random numbers
            __host__ __device__
            Deck(randomGen::Random *t_rand);

            // Copy constructor for decks
            __host__ __device__
            Deck(const Deck &d);

            // Returns the next card in the deck. If no cards
            // are left, returns a "NULL" card.
            __host__ __device__
            cards::Card nextCard();

            // Removes the given card from the deck by 
            // "nullifying" it.
            __host__ __device__
            void removeCard(const cards::Card t_card);

            // Removes an array of cards from the deck by
            // calling removeCard multiple times.
            __host__ __device__
            void removeCards(const cards::Card t_cards[], uint t_length);

            // Returns whether the deck has a next card.
            __host__ __device__
            bool hasNext();

            std::string debugString();
    };
}


#endif
