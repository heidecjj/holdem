// cpuSimulation.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// A CPU implementation of the Simulation interface 
// defined in simulation.h.

#include <cuda_runtime.h>
#include <stdexcept>
#include <iostream>

#include "simulation.h"
#include "cpuSimulation.h"
#include "random.h"
#include "texas.h"
#include "deck.h"
#include "cards.h"

namespace simulation {

    CpuSimulation::CpuSimulation(uint8_t t_numPlayers, uint t_numSims) {
        numPlayers = t_numPlayers;
        numSims = t_numSims;
    }

    float CpuSimulation::winningChance(texas::Hole t_hole, 
            std::vector<cards::Card> t_commSoFar, 
            std::map<texas::HandType, float> *t_handFreq) {
        uint totalWins = 0;
        std::map<texas::HandType, uint> typeCount = createTypeCountMap();
        bool countTypes = (t_handFreq != nullptr);

        randomGen::Random cpuRand = randomGen::Random();

        // Setup what is know about the current deck
        deck::Deck knownDeck(&cpuRand);
        knownDeck.removeCards(t_hole, 2);
        knownDeck.removeCards(&t_commSoFar[0], t_commSoFar.size());

        for (uint i = 0; i < numSims; i++) { // for each simulation
            deck::Deck possibleDeck = knownDeck; // copy the known deck

            // Generate uknown community cards
            auto comm = t_commSoFar;
            while (comm.size() < 5) {
                comm.push_back(possibleDeck.nextCard());
            }

            texas::Hand myHand = texas::findBestHand(&comm[0], t_hole);
            if (countTypes) { // count hand types
                typeCount[myHand.getHandType()]++;
            }

            bool won = true;
            for (auto j = 1; j < numPlayers; j++) {
                // Generate hole cards for opponents and compare hands
                texas::Hole opponentHole{possibleDeck.nextCard(), 
                        possibleDeck.nextCard()}; 
                auto opponentHand = texas::findBestHand(&comm[0], opponentHole);
                
                if (myHand < opponentHand) {
                    won = false;
                    break;
                }
            }

            if (won) {
                totalWins++;
            }
        }

        if (countTypes) {
            for (auto pair: typeCount) {
                (*t_handFreq)[pair.first] = 
                        (float)pair.second / (float)numSims;
            }
        }

        return (float)totalWins / (float)numSims;
    }
    
    float CpuSimulation::winningChance(texas::Hole t_hole, 
            std::vector<cards::Card> t_commSoFar) {
        return winningChance(t_hole, t_commSoFar, nullptr);
    }
}
