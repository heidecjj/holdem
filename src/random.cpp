// random.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Produces random non-negative integers in both
// host code and device code. Handles swappping
// between the random and curand libraries.

#include <random>
#include <curand.h>
#include <curand_kernel.h>

#include "random.h"

namespace randomGen {
    
    Random::Random() {
        #ifndef __CUDA_ARCH__
            std::random_device r;
            lc_engine = std::minstd_rand(r());
        #else
            curand_init(0, 0, 0, &state);
        #endif
    };

    __device__
    Random::Random(int seed) {
        curand_init(seed, 0, 0, &state);
    }

    int Random::nextInt(int t_upperLimit) {
        #ifndef __CUDA_ARCH__
            // Simply generating a number and doing mod t_upperLimit
            // does not give a uniform distribution
            std::uniform_int_distribution<int> uniform_dist(0, t_upperLimit);
            return(uniform_dist(lc_engine));
        #else
            // curand_uniform return a value between 0 and 1
            // but not including 1.
            // Multiplied value will be truncated, so 1 needs
            // to be added to the upper limit.
            return curand_uniform(&state) * (t_upperLimit + 1);
        #endif
    }
}