// gpuSimulation.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// A GPU implementation of the Simulation interface 
// defined in simulation.h.

#include <iostream>
#include <cuda_runtime.h>
#include <climits>

#include "simulation.h"
#include "gpuSimulation.h"
#include "deck.h"
#include "cards.h"
#include "texas.h"
#include "random.h"



namespace simulation {
    __global__ void winningChanceKernel(cards::Card * t_hole, cards::Card *t_comm, 
                int t_commSize, int t_numPlayers, int t_numSims, int t_seed, int *t_wins,
                int *t_typeCount, bool t_countTypes) {
        int index = blockIdx.x * blockDim.x + threadIdx.x;
        if (index >= t_numSims) {
            return;
        }
        
        randomGen::Random gpuRand = randomGen::Random(t_seed + index);

        deck::Deck possibleDeck(&gpuRand);
        possibleDeck.removeCards(t_hole, 2);
        possibleDeck.removeCards(t_comm, t_commSize);

        cards::Card comm[5];
        for (int i = 0; i < t_commSize; i++) {
            comm[i] = t_comm[i];
        }
        for (int j = t_commSize; j < 5; j++) {
            comm[j] = possibleDeck.nextCard();
        }

        texas::Hand myHand = texas::findBestHand(&comm[0], t_hole);
        if (t_countTypes) {
            atomicAdd(t_typeCount + myHand.getHandType(), 1);
        }

        bool won = true;
        for (auto j = 1; j < t_numPlayers; j++) {
            texas::Hole opponentHole{possibleDeck.nextCard(), possibleDeck.nextCard()}; 
            auto opponentHand = texas::findBestHand(&comm[0], opponentHole);
            
            if (myHand < opponentHand) {
                won = false;
                break;
            }
        }

        if (won) {
            atomicAdd(t_wins, 1);
        }
    }
    
    GpuSimulation::GpuSimulation(uint8_t t_numPlayers, uint t_numSims) {
        numPlayers = t_numPlayers;
        numSims = t_numSims;
    }

    float GpuSimulation::winningChance(texas::Hole t_hole, 
            std::vector<cards::Card> t_commSoFar, 
            std::map<texas::HandType, float> *t_handFreq) {
        cards::Card *d_hole, *d_comm;
        int *d_wins;
        int *d_typeCount;
        int h_wins = 0;
        int h_typeCount[NUM_HAND_TYPES] = {0};
        int seed = randomGen::Random().nextInt(INT_MAX);

        bool countTypes = (t_handFreq != nullptr);
        
        // Allocate GPU memory
        cudaMalloc((void **) &d_hole, sizeof(texas::Hole));
        cudaMalloc((void **) &d_comm, sizeof(cards::Card) * t_commSoFar.size());
        cudaMalloc((void **) &d_wins, sizeof(int));
        cudaMalloc((void **) &d_typeCount, sizeof(int) * NUM_HAND_TYPES);

        // Copy memory to GPU
        cudaMemcpy(d_hole, t_hole, sizeof(texas::Hole), cudaMemcpyHostToDevice);
        cudaMemcpy(d_comm, &t_commSoFar[0], sizeof(cards::Card) * t_commSoFar.size(), cudaMemcpyHostToDevice);
        cudaMemcpy(d_wins, &h_wins, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_typeCount, h_typeCount, sizeof(int) * NUM_HAND_TYPES, cudaMemcpyHostToDevice);
        
        // 128 threads/block gave the fastes execution time
        auto numThreads = 128;
        winningChanceKernel<<<(numSims - 1) / numThreads + 1, numThreads>>>(d_hole, d_comm, t_commSoFar.size(), numPlayers, numSims, seed, d_wins, d_typeCount, countTypes);

        cudaDeviceSynchronize();    

        // Copy results back to host
        cudaMemcpy(&h_wins, d_wins, sizeof(int), cudaMemcpyDeviceToHost);
        
        if (countTypes) {
            cudaMemcpy(h_typeCount, d_typeCount, sizeof(int) * NUM_HAND_TYPES, cudaMemcpyDeviceToHost);
            for (int i = texas::kHigh; i <= texas::kRoyalFlush; i++) {
                (*t_handFreq)[i] = (float)h_typeCount[i] / (float)numSims;
            }
        }

        // Free memory
        cudaFree(d_wins);
        cudaFree(d_hole);
        cudaFree(d_comm);
        cudaFree(d_typeCount);
        
        return (float)h_wins / (float)numSims;
    }

    float GpuSimulation::winningChance(texas::Hole t_hole, 
            std::vector<cards::Card> t_commSoFar) {
        return winningChance(t_hole, t_commSoFar, nullptr);
    }
}
