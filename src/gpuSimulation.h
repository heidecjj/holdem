// gpuSimulation.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// A GPU implementation of the Simulation interface 
// defined in simulation.h.

#ifndef GPUSIMULATION_H
#define GPUSIMULATION_H

#include <cuda_runtime.h>

#include "./simulation.h"

namespace simulation {
    class GpuSimulation : public Simulation {
        private:
            uint numSims;

        public:
           GpuSimulation(uint8_t t_numPlayers) : GpuSimulation(t_numPlayers, 10000) {};
           GpuSimulation(uint8_t t_numPlayers, uint t_numSims);
           float winningChance(texas::Hole t_hole, std::vector<cards::Card> t_commSoFar) override;
           float winningChance(texas::Hole t_hole, std::vector<cards::Card> t_commSoFar, std::map<texas::HandType, float> *t_handFreq) override;
    };
}

#endif
