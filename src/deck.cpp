// deck.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Abstracts a standard deck of 52 cards. Cards are dealt
// in a random order and are lazily shuffled. Specific cards
// can also be removed from the deck. See deck.h for details
// on each funtion's purpose.

#include <cuda_runtime.h>
#include <stdexcept>

#include "cards.h"
#include "deck.h"
#include "random.h"

namespace deck {
    Deck::Deck(randomGen::Random *t_rand) {
        rand = t_rand;
        int i = 0, cardNum;
        for (cardNum = 2; cardNum <= cards::kAce; cardNum++) {
            available[i] = cards::Card(cardNum, cards::kClub);
            i++;
        }
        for (cardNum = 2; cardNum <= cards::kAce; cardNum++) {
            available[i] = cards::Card(cardNum, cards::kHeart);
            i++;
        }
        for (cardNum = 2; cardNum <= cards::kAce; cardNum++) {
            available[i] = cards::Card(cardNum, cards::kDiamond);
            i++;
        }
        for (cardNum = 2; cardNum <= cards::kAce; cardNum++) {
            available[i] = cards::Card(cardNum, cards::kSpade);
            i++;
        }
    }

    Deck::Deck(const Deck &d) {
        rand = d.rand;
        for (int i = 0; i < DECK_SIZE; i++) {
            available[i] = d.available[i];
        }
        currentCard = d.currentCard;
    }

    cards::Card Deck::nextCard() {
        skipNullCards();
        if (!hasNext()) {
            #ifndef __CUDA_ARCH__
                throw std::runtime_error("nextCard() cannot be called when there are no cards left.");
            #else
                return cards::Card(0, 0);
            #endif
        }

        // A random swap happens right before returning the next
        // card in the deck so shuffling only happens when
        // necessary.
        auto maxRand = DECK_SIZE - currentCard - 1;
        auto swapIndex = (*rand).nextInt(maxRand);
        
        cards::Card toSwap = available[currentCard + swapIndex];
        available[currentCard + swapIndex] = available[currentCard];
        available[currentCard] = toSwap;

        cards::Card toReturn = available[currentCard++];
        if (toReturn.isNull()) {
            return nextCard();
        }
        return toReturn;
    }

    void Deck::removeCards(const cards::Card t_cards[], uint t_length) {
        for (uint i = 0; i < t_length; i ++) {
            removeCard(t_cards[i]);
        }
    }

    void Deck::removeCard(const cards::Card t_card) {
        for (uint i = 0; i < DECK_SIZE; i++) {
            if (available[i] == t_card) {
                available[i].nullify();
                skipNullCards();
                break;
            }
        }
    }

    void Deck::skipNullCards() {
        while(true) {
            if (hasNext() && available[currentCard].isNull()) {
                currentCard++;
            } else {
                return;
            }
        }
    }

    bool Deck::hasNext() {
        return currentCard < DECK_SIZE;
    }

    std::string Deck::debugString() {
        std::string toReturn = "[";
        for (uint i = 0; i < DECK_SIZE; i++) {
            if (i == currentCard) {
                // | separates dealt cards from un-dealt cards.
                toReturn += "| ";
            }
            toReturn += available[i].toString() + " ";
        }
        if (currentCard == DECK_SIZE) {
            toReturn += "|";
        }
        toReturn += "]\n";
        return toReturn;
    }
}
