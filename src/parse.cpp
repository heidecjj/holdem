// parse.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Collection of functions for parsing cards from the
// command line. See parse.h for details on each funtion's purpose.

#include <regex>
#include <stdexcept>
#include <iostream>
#include <string>
#include <cuda_runtime.h>

#include "parse.h"
#include "cards.h"

namespace parse {
    static cards::Number cardNumberFromString(const std::string t_numberString) {
        std::regex numberR("10|[2-9]");
        if (std::regex_match(t_numberString, numberR)) {
            return stoi(t_numberString);
        } else {
            switch(std::tolower(t_numberString[0])) { // case insensitive
                case 'j': return cards::kJack;
                case 'q': return cards::kQueen;
                case 'k': return cards::kKing;
                case 'a': return cards::kAce;
            }
        }
        // should never get here
        throw std::runtime_error("invalid string format for cardNumberFromString()");
    }

    static cards::Suit cardSuitFromString(const std::string t_suitString) {
        switch(std::tolower(t_suitString[0])) { // case insensitive
            case 'c': return cards::kClub;
            case 'd': return cards::kDiamond;
            case 'h': return cards::kHeart;
            case 's': return cards::kSpade;        
        }
        // should never get here
        throw std::runtime_error("invalid string format for cardSuitFromString()");
    }

    std::vector<cards::Card> cardFromString(const std::vector<std::string> t_strings) {
        std::vector<cards::Card> toReturn;
        for (auto s: t_strings) {
            toReturn.push_back(cardFromString(s));
        }
        return toReturn;
    }

    cards::Card cardFromString(const std::string t_string) {
        // regex for my defined card format
        // parens define groups for regex_match
        std::regex cardR("^(10|[2-9]|[jJkKqQaA])[-]([cCdDhHsS])$");
        std::smatch cardResult;
        if (std::regex_match(t_string, cardResult, cardR)) {
            // cardResult[1] has what matched the first set of parens
            cards::Number cardNumber = cardNumberFromString(cardResult[1]);

            // cardResult[2] has what matched the second set of parens
            cards::Suit cardSuit = cardSuitFromString(cardResult[2]);
            return cards::Card(cardNumber, cardSuit);
        } else {
            std::string errorString = "invalid card format for :" + t_string;
            throw std::runtime_error("invalid card format: " + t_string);
        }
    }

    bool checkDuplicates(const std::vector<cards::Card> t_hole, const std::vector<cards::Card> t_comm) {
        std::vector<cards::Card> all = t_hole;
        for (auto card: t_comm) {
            all.push_back(card);
        }

        for (auto i = 0; i < all.size() - 1; i++) {
            for (auto j = i+1; j < all.size(); j++) {
                if (all[i] == all[j]) {
                    return true;
                }
            }
        }
        return false;
    }
}
