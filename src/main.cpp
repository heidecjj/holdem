// main.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Adapted from Radman Games's "How to use Boost.Program_options" tutoral at
// http://www.radmangames.com/programming/how-to-use-boost-program_options
//
// Command line interface for running the Holdem application.

#include <iostream> 
#include <string> 
#include <cuda_runtime.h>

#include "boost/program_options.hpp" 
#include "simulation.h"
#include "cpuSimulation.h"
#include "gpuSimulation.h"
#include "parse.h"
#include "random.h"
#include "deck.h"

 
namespace 
{ 
    const size_t ERROR_IN_COMMAND_LINE = 1; 
    const size_t SUCCESS = 0; 
    const size_t ERROR_UNHANDLED_EXCEPTION = 2; 
 
} // namespace 
 
int main(int argc, char** argv) { 
    try { 
        /** Define and parse the program options 
         */ 
        namespace po = boost::program_options; 
        po::options_description desc("Options"); 
        desc.add_options() 
            ("basic-gpu,b", po::bool_switch()->default_value(false), "deprecated, use --gpu or -g instead")
            ("gpu,g", po::bool_switch()->default_value(false), "run with gpu implementation")
            ("help", "Print help messages") 
            ("hole,h", po::value<std::vector<std::string> >()->multitoken(),
                "2 hole cards specifed as:\n<card number or letter>-<first letter of suit>\ne.g. 9-D K-H") 
            ("community,c", po::value<std::vector<std::string> >()->multitoken(), 
                "3, 4, or 5 community cards specified in the same way as hole cards")
            ("numberOfSims,n", po::value<uint>()->default_value(10000), 
                "number of simulations to run")
            ("players,p", po::value<uint>()->default_value(2), 
                "number of players, including you, to simulate")
            ("verbose,v", po::bool_switch()->default_value(false), "be verbose (i.e. output chance of getting each hand type)"); 
 
        po::variables_map vm; 
        try { 
            po::store(po::parse_command_line(argc, argv, desc),    
                                vm); // can throw 
 
            /** --help option 
             */ 
            if (vm.count("help")) { 
                std::cout << "holdem Texas Hold 'Em Simulator" << std::endl 
                                    << desc << std::endl; 
                return SUCCESS; 
            } 
 
            po::notify(vm); // throws on error, so do after help in case 
                                            // there are any problems 
        } catch(po::error& e) { 
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
            std::cerr << desc << std::endl; 
            return ERROR_IN_COMMAND_LINE; 
        } 
 
        // application code here
        // read card string vectors 
        std::vector<std::string> holeStrings;
        if (!vm["hole"].empty()){
            holeStrings = vm["hole"].as<std::vector<std::string> >();
        }
        std::vector<std::string> commStrings;
        if (!vm["community"].empty()) {
            if (holeStrings.size() == 0) {
                std::cerr << "ERROR: community cannot be specified if hole is not specified" << std::endl << std::endl;
                return ERROR_IN_COMMAND_LINE;
            }
            commStrings = vm["community"].as<std::vector<std::string> >();
        }

        // check right number of cards in hole and community
        if (holeStrings.size() != 0 && holeStrings.size() != 2) {
            std::cerr << "ERROR: Expected 0 or 2 hole cards but got " << std::to_string(holeStrings.size()) << std::endl << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }
        if (commStrings.size() != 0 && commStrings.size() != 3 && commStrings.size() != 4 && commStrings.size() != 5) {
            std::cerr << "ERROR: Expected 0, 3, 4, or 5 community cards but got " << std::to_string(commStrings.size()) << std::endl << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        std::vector<cards::Card> hole;
        if (holeStrings.size() == 0) {
            randomGen::Random r;
            deck::Deck d = deck::Deck(&r);
            hole.push_back(d.nextCard());
            hole.push_back(d.nextCard());
            std::cout << "random hole: " << hole[0].toString() << " " << hole[1].toString() << std::endl;
        } else {
            hole = parse::cardFromString(holeStrings);
        }
        std::vector<cards::Card> comm = parse::cardFromString(commStrings);

        if (parse::checkDuplicates(hole, comm)) {
            std::cerr << "ERROR: Hole cards and Community cards cannot contain duplicates" << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        simulation::Simulation *simPtr;
        uint players = vm["players"].as<uint>();
        uint numSims = vm["numberOfSims"].as<uint>();
        if (vm["gpu"].as<bool>()) {
            simPtr = new simulation::GpuSimulation(players, numSims);
        } else if (vm["basic-gpu"].as<bool>()) {
            std::cout << "deprecated, use --gpu or -g instead" << std::endl;
            simPtr = new simulation::GpuSimulation(players, numSims);
        } else {
            simPtr = new simulation::CpuSimulation(players, numSims);
        }
        // run the simulation
        std::map<texas::HandType, float> typeFreq;
        std::cout << "chance of winning: " << std::to_string((*simPtr).winningChance(&hole[0], comm, &typeFreq)) << std::endl << std::endl;
        
        delete [] simPtr;

        if (vm["verbose"].as<bool>()) {
            for (auto pair: typeFreq) {
                std::cout << "chance of " << texas::handTypeToString(pair.first) << ": " << std::to_string(pair.second) << std::endl;
            }
            std::cout << std::endl;
        }
    } 
    
    catch(std::exception& e) { 
        std::cerr << "Unhandled Exception reached the top of main: " 
                            << e.what() << ", application will now exit" << std::endl; 
        return ERROR_UNHANDLED_EXCEPTION; 
    } 
 
    return SUCCESS; 
}
