// cpuSimulation.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// A CPU implementation of the Simulation interface 
// defined in simulation.h.

#ifndef CPUSIMULATION_H
#define CPUSIMULATION_H

#include <cuda_runtime.h>

#include "simulation.h"

namespace simulation {
    class CpuSimulation : public Simulation {
        private:
            uint numSims;

        public:
            CpuSimulation(uint8_t t_numPlayers) : CpuSimulation(t_numPlayers, 10000) {};
            CpuSimulation(uint8_t t_numPlayers, uint t_numSims);
            
            float winningChance(texas::Hole t_hole, std::vector<cards::Card> t_commSoFar) override;
            float winningChance(texas::Hole t_hole, std::vector<cards::Card> t_commSoFar, std::map<texas::HandType, float> *t_handFreq) override;
    };
}

#endif
