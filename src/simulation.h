// simulation.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Defines an interface for CPU and GPU Texas Hold 'Em
// simulation algorithms.

#ifndef SIMULATION_H
#define SIMULATION_H

#include <vector>
#include <map>
#include <cuda_runtime.h>

#include "cards.h"
#include "texas.h"

namespace simulation {

    // Interface for CPU and GPU Texas Hold 'Em simulation algorithms.
    class Simulation {
        protected:
            uint8_t numPlayers;

        public:
            // Returns the chance of winning with certain
            // hole cards and a certain set of community cards.
            virtual float winningChance(texas::Hole t_hole,
                    std::vector<cards::Card> t_commSoFar) = 0;

            // Returns the chance of winning with certain
            // hole cards and a certain set of community cards.
            // Also modifies *t_handFreq to contain the frequency
            // that each hand type occurs.
            virtual float winningChance(texas::Hole t_hole,
                    std::vector<cards::Card> t_commSoFar,
                    std::map<texas::HandType, float> *t_handFreq) = 0;

            std::map<texas::HandType, uint> createTypeCountMap() {
                std::map<texas::HandType, uint> toReturn;
                for (uint i = texas::kHigh; i <= texas::kRoyalFlush; i++) {
                    toReturn[i] = 0;
                }
                return toReturn;
            }
    };
}

#endif
