// cards.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Data model for a standard playing card. See cards.h
// for details on each funtion's purpose.

#include <cuda_runtime.h>

#include "cards.h"

namespace cards{
    __host__ __device__
    Card::Card(Number t_number, Suit t_suit) {
		if (t_suit > 3) {
			std::__throw_invalid_argument("invalid card suit");
		}
		if ((t_number < 2 && t_number != 0) || t_number > kAce) {
			std::__throw_invalid_argument("invalid card number");
		}
		number = t_number;
		suit = t_suit;
	}
	
	bool operator == (Card c1, Card c2) {
		return c1.number == c2.number && c1.suit == c2.suit;
	}

	std::string Card::suitString() {
		return cards::suitToString(suit);
	}

	std::string Card::numberString() {
		return cards::numberToString(number);
	}

	std::string Card::toString() {
		return numberString() + "-" + suitString();
	}

	std::string suitToString(Suit t_suit) {
		switch (t_suit) {
			case cards::kDiamond: return "D";
			case cards::kHeart: return "H";
			case cards::kClub: return  "C";
			case cards::kSpade: return "S";
			default: return "?";
		}
	}

	std::string numberToString(Number t_number) {
		if (t_number > 1 && t_number < cards::kJack)
			return std::to_string(t_number);
		
		switch (t_number) {
			case cards::kAce: return "A";
			case cards::kKing: return "K";
			case cards::kQueen: return "Q";
			case cards::kJack: return "J";
			default: break;
		}
		return "?";
	}

	std::string cardsToString(Card t_cards[], int t_length) {
		std::string toReturn = "[";
		for (int i = 0; i < t_length; i++) {
			toReturn += t_cards[i].toString() + ", ";
		}
		toReturn += "]";
		return toReturn;
	}
}
