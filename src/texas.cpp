// texas.cpp
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Contains Texas Hold 'Em game logic including a class
// for representing a 5 card poker hand, Hand, and a
// class for building Hand objects, HandBuilder. See texas.h
// for details on each funtion's purpose.

#include <iostream>
#include <cuda_runtime.h>

#include "texas.h"
#include "cards.h"

namespace texas {
	std::string handTypeToString(HandType t_type) {
		switch (t_type) {
			case kHigh: return "high";
			case kPair: return "pair";
			case kTwoPair: return "two-pair";
			case kThreeKind: return "three";
			case kStraight: return "straight";
			case kFullHouse: return "full";
			case kFlush: return "flush";
			case kFourKind: return "four";
			case kRoyalFlush: return "r-flush";
			case kStraightFlush: return "straight-flush";
			default: return "?";
		}
	}

	uint32_t Hand::computeHandValue() {
		uint32_t handVal = type;
		handVal = handVal * 16 + high1;
		handVal = handVal * 16 + high2;
		handVal = handVal * 16 + holeHigh;
		handVal = handVal * 16 + holeLow;
		return handVal;
	}

	bool texas::Hand::operator>(Hand h2) {
		return computeHandValue() > h2.computeHandValue();
	}

	bool texas::Hand::operator<(Hand h2) {
		return computeHandValue() < h2.computeHandValue();
	}

	bool texas::Hand::operator==(Hand h2) {
		if (type == h2.type
			&& high1 == h2.high1
			&& high2 == h2.high2
			&& holeHigh == h2.holeHigh
			&& holeLow == h2.holeLow)
			return true;
		return false;
	}

	bool operator==(Hand h1, Hand h2) {
		if (h1.type == h2.type
			&& h1.high1 == h2.high1
			&& h1.high2 == h2.high2
			&& h1.holeHigh == h2.holeHigh
			&& h1.holeLow == h2.holeLow)
			return true;
		return false;
	}

	std::string Hand::debugString() const {
		std::string toReturn;
		toReturn = handTypeToString(type) + " " + cards::numberToString(high1) 
				+ " " + cards::numberToString(high2) + " " + cards::numberToString(holeHigh)+ " " + cards::numberToString(holeLow);
		return toReturn;
	}
	void PrintTo(const Hand& hand, ::std::ostream* os) {
		*os << hand.debugString();
	}

	HandBuilder *HandBuilder::setHole(texas::Hole t_hole) {
		if (t_hole[0].getNumber() > t_hole[1].getNumber()) {
			setHoleHigh(t_hole[0].getNumber());
			setHoleLow(t_hole[1].getNumber());
		} else {
			setHoleHigh(t_hole[1].getNumber());
			setHoleLow(t_hole[0].getNumber());
		}
		return this;
	}


	Hand HandBuilder::build() {
		return Hand(type, high1, high2, holeHigh, holeLow);
	}



	Hand findBestHand(Community t_community, Hole t_hole) {
		HandBuilder bestHand = HandBuilder();
		
		// Hole cards used for tie breakers
		bestHand.setHole(t_hole);

		cards::Card allCards[kPoolSize];
		int i;
		for (i = 0; i <kHoleSize; i++) {
			allCards[i] = t_hole[i];
		}
		for ( ; i < kPoolSize; i++) {
			allCards[i] = t_community[i - kHoleSize];
		}
		// Suits only matter if theres a flush
		// Find out if theres a flush first, then do the
		// rest of the logic without suits.
		cards::Suit flushSuit;
		int suitCount = 0;
		if (findFlushSuit(allCards, &flushSuit, &suitCount)) {
			cards::Number singleSuitSorted[7];
			int back = 0;
			for (auto i = 0; i < kPoolSize; i++) {
				if (allCards[i].getSuit() == flushSuit) {
					singleSuitSorted[back] = allCards[i].getNumber();
					back++;
				}
			}
			sortCardNums(singleSuitSorted, suitCount);

			bestHand.setType(texas::kFlush);
			bestHand.setHigh1(singleSuitSorted[suitCount - 1]);

			cards::Number highCard;
			if (findStraight(singleSuitSorted, suitCount, &highCard)) {
				bestHand.setHigh1(highCard);
				if (highCard == cards::kAce) {
					bestHand.setType(texas::kRoyalFlush);
				} else {
					bestHand.setType(texas::kStraightFlush);
				}
			}
		} else {
			cards::Number sorted[kPoolSize];
			for (int i = 0; i < kPoolSize; i++) {
				sorted[i] = allCards[i].getNumber();
			}
			sortCardNums(sorted, kPoolSize);

			cards::Number pairHigh, pairLow, triple, quad;
			findKinds(sorted, &pairHigh, &pairLow, &triple, &quad);

			cards::Number highStraight = 0;

			if (quad != 0) {
				bestHand.setHigh1(quad);
				bestHand.setType(kFourKind);
			} else if (triple != 0 && pairHigh != 0) {
				bestHand.setType(kFullHouse);
				bestHand.setHigh1(triple);
				bestHand.setHigh2(pairHigh);
			} else if (findStraight(sorted, kPoolSize, &highStraight)) {
				bestHand.setType(kStraight);
				bestHand.setHigh1(highStraight);
			} else if (triple != 0) {
				bestHand.setType(kThreeKind);
				bestHand.setHigh1(triple);
			} else if (pairLow != 0) {
				bestHand.setType(kTwoPair);
				bestHand.setHigh1(pairHigh);
				bestHand.setHigh2(pairLow);
			} else if (pairHigh != 0) {
				bestHand.setType(kPair);
				bestHand.setHigh1(pairHigh);
			} else {
				bestHand.setType(kHigh);
				bestHand.setHigh1(sorted[kPoolSize - 1]);
			}
		}

		return bestHand.build();
	}

	void findKinds(cards::Number t_cardNumbers[], cards::Number *t_pairHigh, cards::Number *t_pairLow, 
						cards::Number *t_triple, cards::Number *t_quad) {
		*t_pairHigh = 0;
		*t_pairLow = 0;		
		*t_triple = 0;
		*t_quad = 0;

		int kinds[cards::kAce - 2 + 1] = {0};
		
		for (int i = 0; i < kPoolSize; i++) {
			kinds[t_cardNumbers[i]-2]++;
		}

		cards::Number oldHigh;
		for (int i = 0; i <= cards::kAce - 2; i++) {
			switch (kinds[i]) {
				case 2:
					oldHigh = *t_pairHigh;
					if (i + 2 > oldHigh) {
						*t_pairHigh = i + 2;
						*t_pairLow = oldHigh;
					} else if (i + 2 > *t_pairLow) {
						*t_pairLow = i + 2;
					}
					break;
				case 3:
					if (i + 2 > *t_triple) {
						*t_triple = i + 2;
					}
					break;
				case 4:
					*t_quad = i + 2;
					break;
			}
		}
	}

	bool findFlushSuit(Pool t_cards, cards::Suit *t_suit, int *t_suitCount) {
		int suitCount[4];
		suitCount[cards::kClub] = 0;
		suitCount[cards::kDiamond] = 0;
		suitCount[cards::kHeart] = 0;
		suitCount[cards::kSpade] = 0;

		for (int i = 0; i < kPoolSize; i++) {
			suitCount[t_cards[i].getSuit()]++;
		}

		for (int i = 0; i < 4; i++) {
			if (suitCount[i] >= 5) {
				*t_suit = i;
				*t_suitCount = suitCount[i];
				return true;
			}
		}
		return false;
	}

	// Finds a straight in a sorted array of card numbers
	bool findStraight(cards::Number t_cardNumbers[], int t_length, cards::Number *t_highNumber) {
		if (t_length < 5 
				|| t_cardNumbers[t_length - 1] < 5 
				|| t_cardNumbers[0] > 10) {
			return false;
		}

		// Since t_cardNumbers is sorted, if t_cardNumbers contains an ace,
		// it must be the last element
		cards::Number flushHigh = t_cardNumbers[t_length - 1];
		bool containsAce = flushHigh == cards::kAce;
		int sequentialCount = 1;

		for (int i = t_length - 1; i > 0; --i) {
			if (t_cardNumbers[i - 1] == t_cardNumbers[i]) {
				continue;
			}

			if (t_cardNumbers[i - 1] == t_cardNumbers[i] - 1) {
				sequentialCount++;
			} else {
				sequentialCount = 1;
				flushHigh = t_cardNumbers[i - 1];
				continue;
			}

			if (t_cardNumbers[i - 1] == 2 && containsAce) {
				sequentialCount++;
			}

			if (sequentialCount >= 5) {
				*t_highNumber = flushHigh;
				return true;
			}
		}
		return false;
	}

	void sortCardNums(cards::Number t_cardNums[], int t_length) {
		// Insertion sort
		for (int i = 1; i < t_length; i++) {
			cards::Number current = t_cardNums[i];
			int j = i;
			while (j > 0 && t_cardNums[j - 1] > t_cardNums[j]) {
				cards::Number swap = t_cardNums[j];
				t_cardNums[j] = t_cardNums[j-1];
				t_cardNums[j-1] = swap;
				j--;
			}
			t_cardNums[j] = current;
		}
	}
}
