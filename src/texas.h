// texas.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Contains Texas Hold 'Em game logic including a class
// for representing a 5 card poker hand, Hand, and a
// class for building Hand objects, HandBuilder.

#ifndef TEXAS_H
#define TEXAS_H

#include <stdint.h>
#include <cuda_runtime.h>

#include "cards.h"

#define NUM_HAND_TYPES 11

namespace texas {
	typedef uint8_t HandType;

	// Arrays types for specific sets of cards in Texas Hold 'Em
	static const int kHoleSize = 2;
	static const int kCommunitySize = 5;
	static const int kPoolSize = kHoleSize + kCommunitySize;
	typedef cards::Card Hole[kHoleSize];
	typedef cards::Card Community[kCommunitySize];
	typedef cards::Card Pool[kPoolSize];

	// Constants for hand types
	static const HandType kRoyalFlush = 10;
	static const HandType kStraightFlush = 9;
	static const HandType kFourKind = 8;
	static const HandType kFullHouse = 7;
	static const HandType kFlush = 6;
	static const HandType kStraight = 5;
	static const HandType kThreeKind = 4;
	static const HandType kTwoPair = 3;
	static const HandType kPair = 2;
	static const HandType kHigh = 1;
	static const HandType nullHandType = 0;

	std::string handTypeToString(HandType t_type);

	// Represents a 5 card poker hand
	class Hand {
		private:
			HandType type : 4;
			// High card for the hand type. Example: This Hand
			// object is representing a pair of 5's so type
			// would represent pair and high1 would be 5.
			cards::Number high1 : 4;

			// Second Highest card for the hand type. Example:
			// This Hand object is representing a full house with
			// three 4's and two 10's so type would represent
			// full house, high1 would be 4 and high2 would be 10.
			cards::Number high2 : 4;

			// High and low card in the player's two hole cards.
			// These are used for tie breakers.
			cards::Number holeHigh : 4;
			cards::Number holeLow : 4;

		public:
			__host__ __device__
			Hand(HandType t_type, cards::Number t_high1, cards::Number t_high2, cards::Number t_holeHigh, cards::Number t_holeLow) {
				type = t_type;
				high1 = t_high1;
				high2 = t_high2;
				holeHigh = t_holeHigh;
				holeLow = t_holeLow;
			}

			// creates a null hand
			__host__ __device__
			Hand() {
				type = nullHandType;
				high1 = 0;
				high2 = 0;
				holeHigh = 0;
				holeLow = 0;
			}

			__host__ __device__
			HandType inline getHandType() {return type;}
			
			// Computes a number for comparing Hand objects
			__host__ __device__
			uint32_t computeHandValue();

			__host__ __device__
			bool operator > (Hand h2);
			__host__ __device__
			bool operator == (Hand h2);
			__host__ __device__
			bool operator < (Hand h2);
			__host__ __device__
			friend bool operator == (Hand h1, Hand h2);

			std::string debugString() const;
	};

	// Used for googletest output when a test comparison fails
	void PrintTo(const Hand& hand, ::std::ostream* os);
	
	static Hand kNullHand = Hand();

	// Builder for creating Hand objects incrementally
	class HandBuilder {
		private:
			HandType type{ nullHandType };
			cards::Number high1{ 0 };
			cards::Number high2{ 0 };
			cards::Number holeHigh{ 0 };
			cards::Number holeLow{ 0 };
		
		public:
			__host__ __device__
			Hand build();
			__host__ __device__
			HandBuilder inline *setType(HandType t_type) {
				type = t_type;
				return this;	
			}
			__host__ __device__
			HandBuilder inline *setHigh1(cards::Number t_high1) {
				high1 = t_high1;
				return this;	
			}
			__host__ __device__
			HandBuilder inline *setHigh2(cards::Number t_high2) {
				high2 = t_high2;
				return this;	
			}
			__host__ __device__
			HandBuilder inline *setHoleHigh(cards::Number t_holeHigh) {
				holeHigh = t_holeHigh;
				return this;	
			}
			__host__ __device__
			HandBuilder inline *setHoleLow(cards::Number t_holeLow) {
				holeLow = t_holeLow;
				return this;	
			}
			__host__ __device__
			HandBuilder *setHole(texas::Hole t_hole);
	};

	// Returns a Hand object with the best 5 card poker
	// hand that can be formed from the provided
	// community and hole cards.
	__host__ __device__
	Hand findBestHand(Community t_community, Hole t_hole);

	// Returns true if a flush was found. *t_suit will contain suit of
	// the found flush and *t_suit_count will contain the number of
	// cards involved in the flush.
	__host__ __device__
	bool findFlushSuit(Pool t_cards, cards::Suit *t_suit, int *t_suitCount);

	// Returns true if a straight was found in t_cardNumbers[] which
	// must be sorted from lowest to highest. *t_highNumber will contain
	// the highest number involved in the found straight. t_lenght is the
	// number of cards in the t_cardNumbers array.
	__host__ __device__
	bool findStraight(cards::Number t_cardNumbers[], int t_length, cards::Number *t_highNumber);
	
	// Finds the highest pairs, triples, and quadruples from an array
	// of 7 card numbers. Since this method is for 5 card poker hands,
	// no more than 2 pairs can be found. *t_pairHigh, *t_pairLow,
	// *t_triple, and *t_quad will contain the card numbers for the highest
	// pair, second highest par, highest triple, and highest quadruple
	// respectively.
	__host__ __device__
	void findKinds(cards::Number t_cardNumbers[], cards::Number *t_pairHigh, cards::Number *t_pairLow,
						 cards::Number *t_triple, cards::Number *t_quad);

	// Sorts the given card numbers in place from lowest to highest
	__host__ __device__
	void sortCardNums(cards::Number t_cardNums[], int t_length);
}

#endif
