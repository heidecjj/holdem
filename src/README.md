# Holdem src [![pipeline status](https://gitlab.com/heidecjj/holdem/badges/master/pipeline.svg)](https://gitlab.com/heidecjj/holdem/commits/master)
This folder contains all the source files for Holdem.

## Design
Below is a simplified UML diagram depicting how different components interact in Holdem.

![UML](http://www.plantuml.com/plantuml/png/hLAnQiCm5Dpz5OzC2IGE3NGD8IaqG-cIqALqaENdM5IIZVJKf9J-UorPDi9JeLqit-dkVC_IpX8pv9JCk6JMme6P4huo02SqmRDJ1Pe1dPoWx3iA7v2_Us5mlBu1xnOR8MYyaN-PBpfzKAvMszdzPXQsinXnnXkp8UF8T0cLqEKUBNaGZFr-dc-3Au5UCO5x9sI99aO7Qam-VxO8r3s6GMfngUzIi1b18_7O4Ib-QgunQEv3bhszNpRBqR8Sn8l1p1lLEaBlV67IeHygBHWx_t_F63nVH0o59m9Shy4Y4HeBLMC06Qz1eJzb-616i4Bwh-Tvxgkc9SV_dWdTsIl64Kv2ESb8D3hCKCc64Lo5ra9V3ZNJl1yvaGs7EUNy7PbouO0dPAmGxshhabsXMeaADTdVzeenZt-CsQ4kdP8_)
