// random.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Produces random non-negative integers in both
// host code and device code. Handles swappping
// between the random and curand libraries.

#ifndef RANDOM_H
#define RANDOM_H

#include <random>
#include <curand.h>
#include <curand_kernel.h>
#include <cuda_runtime.h>


namespace randomGen {
    class Random {
        private:
            #ifndef __CUDA_ARCH__ // only defined for host code
                std::minstd_rand lc_engine;
            #endif 
            curandState_t state;

        public:
            __host__ __device__
            Random();

            __device__
            Random(int seed);

            // returns a random integer between 0 and t_upperLimit
            // inclusive.
            __host__ __device__
            int nextInt(int t_upperLimit);
    };
}

#endif