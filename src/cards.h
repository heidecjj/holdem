// cards.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Data model for a standard playing card.

#ifndef CARDS_H
#define CARDS_H

#include <cuda_runtime.h>
#include <stdint.h>
#include <string>

namespace cards {
	typedef uint8_t Suit;
	typedef uint8_t Number;

	// Data model for a standard playing card.
	class Card {
		private:
			Suit suit : 2;
			Number number : 6;

		public:
			// Constructor for a "NULL" card
			__host__ __device__
			Card() : Card(0, 0) {};

			// Constructs a card with the given number and suit
            __host__ __device__
			Card(Number t_number, Suit t_suit);

			__host__ __device__
			Number inline getNumber() {
				return number;
			}
			
			__host__ __device__
			Suit inline getSuit() {
				return suit;
			}

			// "NULL" cards are defined to have a suit = 0 and
			// number = 0.
			__host__ __device__
			bool inline isNull() {
				return (suit == 0 && number == 0);
			}

			// Makes this card a "NULL" card.
			__host__ __device__
			void inline nullify() {
				suit = 0;
				number = 0;
			}

			std::string toString();
			std::string suitString();
			std::string numberString();
			__host__ __device__
			friend bool operator == (Card c1, Card c2);
	};

	std::string suitToString(Suit t_suit);
	std::string numberToString(Number t_number);
	std::string cardsToString(Card t_cards[], int t_length);

	// findFlushSuit in texas.cpp depends on these numbers in order
	// to avoid using std::map
	// Constants for each suit
	static const uint8_t kDiamond = 0;
	static const uint8_t kHeart = 1;
	static const uint8_t kClub = 2;
	static const uint8_t kSpade = 3;

	// findKinds in texas.cpp depends on these numbers in order
	// to avoid using std::map
	// Constants for face cards and Ace
	static const uint8_t kAce = 14;
	static const uint8_t kKing = 13;
	static const uint8_t kQueen = 12;
	static const uint8_t kJack = 11;
}

#endif //CARDS_H
