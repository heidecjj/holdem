// parse.h
// Author: josheidecker@gmail.com (Josh Heidecker)
// 
// Collection of functions for parsing cards from the
// command line.

#ifndef PARSE_H
#define PARSE_H

#include <vector>
#include <cuda_runtime.h>

#include "cards.h"

namespace parse {
    // Converts a vector of strings to a vector of Cards.
    std::vector<cards::Card> cardFromString(const std::vector<std::string> t_strings);

    // Converts a string, formatted as <1-9, j, J, q, Q, k, K, a, A>-<c, C, d, D, h, H, s, S>, to a Card.
    cards::Card cardFromString(const std::string t_string);

    // Checks for any duplicate cards given a player's
    // hole and the community cards.
    bool checkDuplicates(const std::vector<cards::Card> t_hole, const std::vector<cards::Card> t_comm);
}


#endif
